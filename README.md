<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="150" alt="Nest Logo" /></a>
</p>


# Botleirplate

Un boilerplate sobre NestJS y TypeScript para creación de bots de mazmo.

## En que te va a ayudar?

Botleirplate implementa las funciones mas comunes para interactuar con la API de mazmo, como ser:
 
- Obtener información de un usuario
- Recibir mensajes de la sala
- Enviar mensajes a una sala
- Enviar notificaciones a un usuario en particular dentro de una sala
- Requerir, recibir y enviar transferencias de sades
- Obtener el balance de sades de tu bot
- Sistema de registro y ejecución de "comandos" que se dispararán automáticamente al recibir un mensaje específico
- Además, declaración de los `type` mas comunes para facilitar el desarrollo de tu bot!

## Qué onda esos comandos?
Los comandos son clases que implementan la interface `commandHandler`, estos encapsulan la funcionalidad específica de una acción a realizar, mantieniendo el código conciso, organizado y testeable.

Para cada comando podés especificar una cadena disparadora, la cual al ser identificada por el endpoint de mensajes éste transferirá el control de la ejecución hacia el manejador adecuado. 

Mirá en la carpeta `commandHandlers` para algunos ejemplos.

**Importante:** Para que todo esto funcione, el hook de mensajes debe que apuntar al endpoint que ejecutará `AppController.onRoomMessage()`, lee mas adelante acerca de los endpoints preexistentes en Botleirplate.
 
   

## You son of a bitch, I'm in... Cómo creo y registro nuevos comandos?
Para agregar tu nuevo comando deberás seguir los siguientes pasos:

- Creá una clase que sea `@Injectable` que implemente la interface `CommandHandler`
- Especificá cuál va a ser la cadena disparadora (con la que debe comenzar el mensaje) del comando en la función `getSignature()`, esta cadena puede ser cualquier cadena alfanumérica o caracteres de puntuación, **no puede contener espacios**. 
- Codificá la lógica de tu comando en `handleCommand()`
- Agregá tu clase en la sección `providers` de `app.module`
- En `CommandService` agregá tu clase al constructor (esto injecta la instancia por DI) y en el mismo constructor agregá la llamada a `this.registerHandler(tuNuevoHandler)` tal como se muestra con los handlers de ejemplo existentes

*Tip: utilizá uno de los comandos de ejemplo para crear tu nuevo comando* 

**Opcional:** podés especificar un prefijo disparador previo en `CommandService.commandActivationPrefix`, de esta manera tu bot solo evaluará comandos cuando los mensajes comiencen con la cadena especificada en `commandActivationPrefix`.

Ejemplo: suponiendo que existe el comando `/echo`, por defecto los siguientes mensajes serán ruteados al comando `/echo`:

`/echo este es el texto que se pasará al comando`
 
Si asignás el valor `mibot` a `commandActivationPrefix`, sólo los siguientes mensajes que comiencen con `mibot` serán evaluados, como por ejemplo:

`mibot /echo este es el texto que se pasará al comando`  


## Endpoints preexistentes en Botleirplate

La clase `AppController` ubicada en `app.controller` posee los siguientes endpoints:

- `/message`: Recibe nuevos mensajes de la sala. Este endpoint integra el servicio `CommandService` que verifica si se debe disparar algún comando.  

- `/sades_received`: Recibe notificaciones al recibir una transferencia de sades.

- `/user_enter`: Recibe notificaciones al ingresar un nuevo usuario en la sala.

- `/user_leave`: Recibe notificaciones al salir un usuario de la sala.

- `/new_ban`: Recibe notificaciones al banear un usuario en la sala.

- `/channel_updated`: Recibe notificaciones al actualizar la información del canal.

- `/message_updated`: Recibe notificaciones al editar un mensaje.

- `/reaction_added`: Recibe notificaciones al agregar una reacción.

- `/reaction_removed`: Recibe notificaciones al quitar una reacción.


Podés ajustar el nombre de los endpoints modificando el decorador que antecede a cada función, como por ejemplo: `@Post('message')`.

Podés leer más acerca de controladores en el sitio oficial de [NestJS](https://docs.nestjs.com/).


## Instalación del entorno

```bash
$ npm install
```

## Configuración del bot-secret y ID del bot owner

En el archivo `.env` ubicado en la raíz del proyecto debés especificar los siguientes parámetros 
```
BOT_SECRET=<bot secret provisto por mazmo>
OWNER_ID=<owner id>
```

El `OWNER_ID` referencia al ID de usuario dueño del bot, este puede ser utilizar para restringir comandos que sólo vos podés ejecutar en tu bot, mirá el `BalanceHandler` para un ejemplo de esto.

Para averiguar tu ID de usuario logueate a mazmo, navegá cualquier página privada, abrí DevTools y en el tab Network buscá la llamada a `https://production.mazmoapi.net/users/me`, la propiedad `id` del payload devuelto es tu ID de usuario.


## Organización del código

- `/commandHandlers` en esta carpeta se encuentran algunos manejadores de comandos de bot de ejemplo (echo, requerir sades, obtener balance de sades del bot)
- `/middleware` en esta carpeta se encuentra el middleware de verificación de header de bot-secret
- `/service` en esta carpeta se encuentra el servicio BotService que implementa las funciones mas frecuentes para interactuar con el backend y el servicio CommandService de ruteo de comandos  
- `app.controller.ts` implementa los endpoints que se deben especificar en cada URL de los hooks del bot
- `app.module.ts` declaración del NestModule
- `main.ts` entry point del bot
- `types.ts` declaración de tipos e interfaces

Podés leer más acerca de las utilidades y servicios de NestJS en el sitio oficial de [NestJS](https://docs.nestjs.com/).

## Ejecutando el bot de manera local

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```


## Probando tu bot ejecutándolo de manera local. Si, LOCALMENTE

Con la ayuda de [ngrok](https://ngrok.com/) podés lograr esto, creá una cuenta e instalá el cliente. Esta herramienta sirve para exponer, mediante una conexión a sus servidores, un puerto local mediante un URI temporal.

Tenés que ejecutar el bot de manera local (en modo development es sufuciente como se explica en el punto anterior), asumiendo que no cambiaste el puerto, desde la consola de sistema ejecutá:

```
$ ngrok http 3000
```

Vas a ver algo como lo siguiente: 

![ngrok](./ngrok.jpg)

Lo que acabás de hacer es crear un "tunel" desde las URL que figuran como "Forwarding" hacia el puerto 3000 de tu equipo local, utilizando el protocolo http. Podés utilizar tanto la URL segura como la insegura para fines de prueba, es indistinto. 

*Tip: accediendo desde un navegador a la URL que se lista como "Web Interface" en `ngrok` vas a poder visualizar las llamadas entrantes junto a sus respectivos payloads y encabezados, lo cual es útil si necesitás ver qué estás recibiendo desde la API de mazmo.*  

Debés copiar la URL (http://31df01159843.ngrok.io/ en este caso, vos tenés que usar la que te provea ngrok al ejecutarlo) y utilizarla para configurar tu bot de prueba en mazmo, te debería quedar de la siguiente manera:

![ngrok](./bothooksconfig.jpg)

Grabás la configuración de los hooks de tu bot y comenzará a recibir automáticamente los eventos de los canales en los que está instalado! 

## Publicar en heroku
La primera vez se deberá inicializar un repositorio git y loguear a Heroku 
```bash
$ heroku login
$ git init
$ heroku git:remote -a <nombre_de_la_app_en_heroku>
$ git add .
$ git commit -m "Initial commit"
$ git push heroku master
```

Luego de esto, para posteriormente actualizar la aplicación (deberá hacerce un `git commit` previamente), ejecutar:
```bash
$ git push heroku master
```

## Ejecutar tests unitarios, e2e y de coverage

Para correr los tests de tu bot, así como los tests incluídos en Botleirplate tenés que ejecutar:

![tdd](./tdd.png)

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
