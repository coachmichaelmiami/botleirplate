import { HttpService, Injectable } from '@nestjs/common';
import { AxiosRequestConfig } from 'axios';
import { AnyDict, RoomReplyMessage, SadesAsk, UserData, UserNotify } from '../types';

@Injectable()
export class BotService {
    constructor(private httpService: HttpService) {}


    /**
     * Envía un mensaje a un canal.
     * @param string replyKey
     * @param string channelId
     * @param string replyPayload Especificar en rawContent el mensaje a enviar a la sala, otras propiedades son opcionales y dependen del tipo de mensaje a enviar
     */
    private async sendMessageToChannel(replyKey: string, channelId: string, replyPayload: RoomReplyMessage) {
        const postbackUrl = `https://production.mazmoapi.net/chat/channels/${channelId}/messages`
        const config: AxiosRequestConfig = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Bot-Key': replyKey,
            },
        }
        await this.httpService.post(postbackUrl, replyPayload, config).toPromise().catch(e => true)
    }


    /**
     * Devuelve los datos de un usuario
     * @param userId
     */
    async getUserData(userId: number): Promise<UserData> {
        const res = await this.httpService.get('https://production.mazmoapi.net/users?ids=' + userId).toPromise().catch(e => { return { data: {} } })
        if (res.data[userId.toString()]) {
            return res.data[userId.toString()] as UserData
        }
        else {
            return null
        }
    }

    /**
     * Devuelve el balance de sades del bot
     */
    async getBalance(): Promise<number> {
        const config: AxiosRequestConfig = {
            params: { botSecret: process.env.BOT_SECRET }
        }
        const { data: { balance } } = await this.httpService.get('https://production.mazmoapi.net/bank/boxes/balance', config).toPromise().catch(e => { return { data: {balance: 0} } })
        return balance ?? 0
    }

    /**
     * Envía una notificación a un canal para un usuario específico.
     * Solo el usuario destinatario podrá ver el mensaje.
     * @param replyKey
     * @param channelId
     * @param toUserId
     * @param rawContent Mensaje a enviar a la sala, acepta el mismo markdown que la UI del chat
     */
    async notifyUser(replyKey: string, channelId: string, toUserId: number, rawContent: string) {
        const notification: UserNotify = {
            type: 'NOTICE',
            toUserId: toUserId,
            rawContent
        }

        await this.sendMessageToChannel(replyKey, channelId, notification)
    }

    /**
     * Envía un pedido de transferencia de sades a un canal.
     * @param replyKey
     * @param channelId
     * @param rawContent Mensaje a enviar a la sala, acepta el mismo markdown que la UI del chat
     * @param amount Cantidad de sades a pedir
     * @param fixed N/A
     * @param transferData Información que será enviada por mazmo al recibir sades
     */
    async requestSades(replyKey: string, channelId: string, rawContent: string, amount: number, fixed: boolean, transferData?: AnyDict) {
        const replyPayload: SadesAsk = {
            rawContent,
            sadesAsk: {
                amount,
                fixed,
            }
        }
        if (transferData) {
            replyPayload.sadesAsk.transferData = transferData
        }

        await this.sendMessageToChannel(replyKey, channelId, replyPayload)
    }

    async transferSadesToUser(toUserId: number, concept: string, amount: number) {
        const config: AxiosRequestConfig = {
            params: { botSecret: process.env.BOT_SECRET }
        }
        const payload = {
            to: { type: 'USER', id: toUserId },
            concept,
            amount,
            data: {},
        }
        await this.httpService.post('https://production.mazmoapi.net/bank/transactions', payload, config).toPromise().catch(e => true)
    }

    /**
     * Envía un mensaje a un canal.
     * @param string replyKey
     * @param string channelId
     * @param string replyPayload Mensaje a enviar a la sala, acepta el mismo markdown que la UI del chat
     */
    async sendReply(replyKey: string, channelId: string, replyMessage: string) {
        await this.sendMessageToChannel(replyKey, channelId, { rawContent: replyMessage })
    }

    /**
     * Devuelve un objeto con el channelId y el replyKey para ser utilizado por defecto en el transferData de los pedidos de sades
     * @param replyKey
     * @param channelId
     * @param extraPayload Opcional. Objeto con propiedades extras a ser añadidas al objecto devuelto
     */
    getTransferData(replyKey: string, channelId: string, extraPayload?: AnyDict): AnyDict {
        return {
            replyKey,
            channelId,
            ...extraPayload
        }
    }
}
