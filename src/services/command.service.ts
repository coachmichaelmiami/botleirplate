import { Injectable } from '@nestjs/common';
import { Request, Response } from 'express';
import { CommandHandler } from '../types';
import { EchoHandler } from '../commandHandlers/echo';
import { BalanceHandler } from '../commandHandlers/balance';
import { DonateHandler } from '../commandHandlers/donate';
import { TransferHandler } from '../commandHandlers/transfer';

@Injectable()
export class CommandService {
    /**
     * Prefijo de activación para los comandos, en caso de especificarse, los comandos handlers de comandos
     * solamente se dispararán si el mensaje comienza con commandActivationPrefix, en este caso el comando será la
     * segunda cadena (tomando el espacio como separador) identificada, ejemplo de interpretación al especificar prefijo:
     * <prefijo> <comando> <mensaje>
     */
    private commandActivationPrefix = ''

    /**
     * CommandHandlers registrados en el bot. NO modificar manualmente, inyectarlos y registrarlos en el constructor
     * mediante la llamada a `this.registerHandler(commandHandler)`
     */
    private handlers: { [key: string]: CommandHandler } = {};

    constructor(
        private readonly echoHandler: EchoHandler,
        private readonly balanceHandler: BalanceHandler,
        private readonly donateHandler: DonateHandler,
        private readonly transferHandler: TransferHandler,
    ) {
        this.registerHandler(echoHandler)
        this.registerHandler(balanceHandler)
        this.registerHandler(donateHandler)
        this.registerHandler(transferHandler)
    }

    private registerHandler(handler: CommandHandler) {
        this.handlers[handler.getSignature()] = handler;
    }

    async handle(rawContent: string, req: Request, res: Response): Promise<boolean> {
        const messageParts = rawContent.split(' ');
        let command = messageParts[0]
        if (this.commandActivationPrefix) {
            // remover prefijo
            messageParts.splice(0, 1)

            // verificar que haya al menos un comando y el prefijo de activación
            if (messageParts.length && (this.commandActivationPrefix === command)) {
                command = messageParts[0]
            }
            else {
                // resetear comando
                command = null
            }
        }

        if (command && this.handlers[command]) {
            // remover comando
            messageParts.splice(0, 1)

            // se ha encontrado coincidencia para un comando registrado
            await this.handlers[command].handleCommand(req, res, messageParts.join(' '));
            return true
        }
        else {
            return false
        }
    }
}
