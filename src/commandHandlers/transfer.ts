import { CommandHandler, RoomMessage } from '../types';
import { Request, Response } from 'express';
import { BotService } from '../services/bot.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class TransferHandler implements CommandHandler {
    constructor(private readonly botService: BotService) {
    }

    getSignature(): string {
        return '/transfer';
    }

    async handleCommand(req: Request, res: Response, message: string) {
        const body = req.body as RoomMessage
        if (body.message.author.id.toString() === process.env.OWNER_ID) {
            if (!isNaN(Number(message)) && (Number(message) > 0)) {
                const amount = Number(message)
                await this.botService.transferSadesToUser(Number(process.env.OWNER_ID), 'Sades withdrawl', amount)
                await this.botService.notifyUser(body.key, body.message.channel.id, body.message.author.id, `Transferidos **${amount.toFixed(2)}** sades!`);
            }
        } else {
            await this.botService.notifyUser(body.key, body.message.channel.id, body.message.author.id, `No te hagas el piola... solo mi Amo puede recibir mis sades!`);
        }
    }

}
