import { CommandHandler, RoomMessage } from '../types';
import { Request, Response } from 'express';
import { Injectable } from '@nestjs/common';
import { BotService } from '../services/bot.service';

@Injectable()
export class EchoHandler implements CommandHandler {
    constructor(private readonly botService: BotService) {
    }

    getSignature(): string {
        return '/echo';
    }

    async handleCommand(req: Request, res: Response, message: string) {
        if (message) {
            const body = req.body as RoomMessage
            await this.botService.sendReply(body.key, body.message.channel.id, message)
        }
    }
}
