import { CommandHandler, RoomMessage } from '../types';
import { Request, Response } from 'express';
import { BotService } from '../services/bot.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class DonateHandler implements CommandHandler {
    constructor(private readonly botService: BotService) {
    }

    getSignature(): string {
        return '/donate';
    }

    async handleCommand(req: Request, res: Response, message: string) {
        if (!isNaN(Number(message)) && (Number(message) > 0)) {
            const body = req.body as RoomMessage
            await this.botService.requestSades(body.key, body.message.channel.id, `Copate y dona **${message}** sades a Botito\nGracias!`, Number(message), false, this.botService.getTransferData(body.key, body.message.channel.id));
        }
    }

}
