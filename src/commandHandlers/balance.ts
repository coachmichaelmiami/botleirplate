import { CommandHandler, RoomMessage } from '../types';
import { Request, Response } from 'express';
import { BotService } from '../services/bot.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class BalanceHandler implements CommandHandler {
    constructor(private readonly botService: BotService) {
    }

    getSignature(): string {
        return '/balance';
    }

    async handleCommand(req: Request, res: Response, message: string) {
        const body = req.body as RoomMessage
        if (body.message.author.id.toString() === process.env.OWNER_ID) {
            const balance = await this.botService.getBalance();
            await this.botService.notifyUser(body.key, body.message.channel.id, body.message.author.id, `Tengo encanutados **${balance.toFixed(2)}** sades!`);
        } else {
            await this.botService.notifyUser(body.key, body.message.channel.id, body.message.author.id, `No te hagas el piola... solo mi Amo puede ver cuanto recolecté`);
        }
    }

}
