import { Body, Controller, Get, HttpException, NotFoundException, Post, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { AnyDict, RoomMessage, SadesReceivedTransaction, UserData } from './types';
import { CommandService } from './services/command.service';
import { BotService } from './services/bot.service';

@Controller()
export class AppController {

    constructor(
        private readonly botService: BotService,
        private commandService: CommandService
    ) {
    }

    /**
     * Endpoint ejecutado al recibir mensaje de la sala, incluye la lógica necesaria para ejecutar los
     * commandHandlers registrados en el constructor
     */
    @Post('message')
    async onRoomMessage(@Body() body: RoomMessage, @Req() req: Request, @Res() res: Response) {
        if (! await this.commandService.handle(body.message.payload.rawContent, req, res)) {
            // no se ha encontrado coincidencia para un comando registrado
            // throw new HttpException('No handler defined for command ' + command, 500)
        }

        res.status(200).send('OK')
    }


    /**
     *  Endpoint ejecutado al recibir una transferencia de sades entrante
     */
    @Post('sades_received')
    async onSadesReceived(@Body() body: SadesReceivedTransaction, @Req() req: Request, @Res() res: Response) {
        const user: UserData = await this.botService.getUserData(body.transaction.from.owner.id);
        await this.botService.sendReply(body.transaction.data.replyKey, body.transaction.data.channelId, `Gracias @${user.username ?? 'usuario no identificado'} por tu infinita generosidad!`);
        res.status(200).send('OK')
    }


    /**
     *  Endpoint ejecutado al ingresar un nuevo usuario en la sala
     */
    @Post('user_enter')
    async onUserEnter(@Body() body: RoomMessage, @Req() req: Request, @Res() res: Response) {
        res.status(200).send('OK')
    }

    /**
     *  Endpoint ejecutado al salir un usuario de la sala
     */
    @Post('user_leave')
    async onUserLeave(@Body() body: RoomMessage, @Req() req: Request, @Res() res: Response) {
        res.status(200).send('OK')
    }

    /**
     *  Endpoint ejecutado al banear un usuario en la sala
     */
    @Post('new_ban')
    async onNewBan(@Body() body: RoomMessage, @Req() req: Request, @Res() res: Response) {
        res.status(200).send('OK')
    }

    /**
     *  Endpoint ejecutado al actualizar la información del canal (se dispara cuando se remueve un baneo)
     */
    @Post('channel_updated')
    async onChannelUpdated(@Body() body: RoomMessage, @Req() req: Request, @Res() res: Response) {
        res.status(200).send('OK')
    }

    /**
     *  Endpoint ejecutado al editar un mensaje
     *  Nota: at the time of this writing este evento no es disparado por el backend de mazmo
     */
    @Post('message_updated')
    async onMessageUpdated(@Body() body: AnyDict, @Req() req: Request, @Res() res: Response) {
        // @TODO: to implement
        res.status(200).send('OK')
    }

    /**
     *  Endpoint ejecutado al agregar una reacción
     *  Nota: at the time of this writing este evento no es disparado por el backend de mazmo
     */
    @Post('reaction_added')
    async onReactionAdded(@Body() body: AnyDict, @Req() req: Request, @Res() res: Response) {
        // @TODO: to implement
        res.status(200).send('OK')
    }

    /**
     *  Endpoint ejecutado al quitar una reacción
     *  Nota: at the time of this writing este evento no es disparado por el backend de mazmo
     */
    @Post('reaction_removed')
    async onReactionRemoved(@Body() body: AnyDict, @Req() req: Request, @Res() res: Response) {
        // @TODO: to implement
        res.status(200).send('OK')
    }


    /**
     * Ruta por defecto
     */
    @Post('*')
    defaultRoute(@Body() body: any, @Req() req: Request, @Res() res: Response) {
        throw new NotFoundException()
    }
}
