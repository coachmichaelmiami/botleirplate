import { HttpModule, MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { BotService } from './services/bot.service';
import { ConfigModule } from '@nestjs/config';
import { EchoHandler } from './commandHandlers/echo';
import { BalanceHandler } from './commandHandlers/balance';
import { DonateHandler } from './commandHandlers/donate';
import { BotRequestMiddleware } from './middleware/botRequest';
import { CommandService } from './services/command.service';
import { TransferHandler } from './commandHandlers/transfer';

@Module({
    imports: [
        ConfigModule.forRoot(),
        HttpModule
    ],
    controllers: [
        AppController
    ],
    providers: [
        BotService,
        CommandService,
        EchoHandler,
        BalanceHandler,
        DonateHandler,
        TransferHandler
    ],
})
export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        // configura el middleware de verificación de header para el controlador principal
        consumer.apply(BotRequestMiddleware).forRoutes(AppController)
    }
}

