import { HttpException, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

@Injectable()
export class BotRequestMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: NextFunction) {
        if (req.headers['bot-secret'] !== process.env.BOT_SECRET) {
            throw new HttpException('Forbidden', 403)
        }
        next();
    }
}
